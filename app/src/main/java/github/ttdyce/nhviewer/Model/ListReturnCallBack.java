package github.ttdyce.nhviewer.Model;

import java.util.ArrayList;

public interface ListReturnCallBack {
    void onResponse(ArrayList list);
}
